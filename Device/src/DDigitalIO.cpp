
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DDigitalIO.h>
#include <ASDigitalIO.h>


#include <LogIt.h>


namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DDigitalIO::DDigitalIO (
    const Configuration::DigitalIO & config,
    Parent_DDigitalIO * parent
):
    Base_DDigitalIO( config, parent),
	m_sca( parent->getParent()->sca() ),
	m_id( config.id() ),
	m_isInput( config.isInput())

    /* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DDigitalIO::~DDigitalIO ()
{
}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DDigitalIO::readValue (
    OpcUa_Boolean &value,
    UaDateTime &sourceTime
)
{
    sourceTime = UaDateTime::now();
    value = m_sca.gpio().getRegisterBitValue(Sca::Constants::GpioRegisters::DataIn,m_id);
	LOG(Log::INF) << " >>>>>> read ="  << (value ? "1" : "0") << " from gpio pin #"<<unsigned(m_id);
    return OpcUa_Good;
}

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DDigitalIO::writeValue (
    OpcUa_Boolean &value
)
{
	if(m_isInput)
	{
		LOG(Log::ERR) << "GPIO Line is config'd as INPUT. Can't write.";
		return OpcUa_Bad;
	}

	m_sca.gpio().setRegisterBitToValue(Sca::Constants::GpioRegisters::DataOut,m_id,value);
	LOG(Log::INF) << " >>>>>> writing = "  << (value ? "1" : "0") << " to gpio pin #"<<unsigned(m_id);
    return OpcUa_Good;
}



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

void Device::DDigitalIO::updateOutgoingValue()
{
	//auto value = m_sca.gpio().getRegister(::Sca::Constants::GpioRegisters::DataOut);
	//m_addressSpaceLink->setValue( value, OpcUa_Good );
}

void DDigitalIO::setAsInput()
{
	m_sca.gpio().setRegisterBitToValue(::Sca::Constants::GpioRegisters::Direction,m_id,false);
}

void DDigitalIO::setAsOutput()
{
	m_sca.gpio().setRegisterBitToValue(::Sca::Constants::GpioRegisters::Direction,m_id,true);
}

bool DDigitalIO::readInputValue()
{
	return m_sca.gpio().getRegisterBitValue(::Sca::Constants::GpioRegisters::Direction,m_id);
}

}


