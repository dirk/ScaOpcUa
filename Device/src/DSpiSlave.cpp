
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DSpiSlave.h>
#include <ASSpiSlave.h>


#include <LogIt.h>


namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DSpiSlave::DSpiSlave (
    const Configuration::SpiSlave & config,
    Parent_DSpiSlave * parent
):
    Base_DSpiSlave( config, parent),
	m_slaveId( config.slaveId() ),
	m_transmissionBaudRate( config.busSpeed() ),
	m_transmissionBitLength( config.transmissionSize() ),
	m_sclkIdleHigh( config.sclkIdleHigh() ),
	m_sampleAtFallingRxEdge( config.sampleAtFallingRxEdge() ),
	m_sampleAtFallingTxEdge( config.sampleAtFallingTxEdge() ),
	m_lsbToMsb( config.lsbToMsb() ),
	m_autoSsMode( config.autoSsMode() )

    /* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DSpiSlave::~DSpiSlave ()
{
}

/* delegators for cache variables and external variables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DSpiSlave::readWrite (
    UaByteString &value,
    UaDateTime &sourceTime
)
{
    sourceTime = UaDateTime::now();
    return OpcUa_BadNotImplemented;
}

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DSpiSlave::writeWrite (
    UaByteString &value
)
{
	Sca::Sca &sca = getParent()->getParent()->sca();
	LOG(Log::INF) << "SPI write on " << getFullName() << ", slaveId=" << (int)m_slaveId << " length=" << value.length() << " bytes.";
	std::vector<uint8_t> bytes ( value.data(), value.data()+value.length() );
	try
	{
		sca.spi().configureBus( m_transmissionBaudRate, m_transmissionBitLength );
		sca.spi().setSsMode( m_autoSsMode );

		sca.spi().setTxEdge( m_sampleAtFallingTxEdge );
		sca.spi().setRxEdge( m_sampleAtFallingRxEdge );
		sca.spi().setLsbToMsb( m_lsbToMsb );
		sca.spi().setInvSclk( m_sclkIdleHigh );

		sca.spi().writeSlave(m_slaveId, bytes);
	}
	catch (const std::exception& e)
	{
		LOG(Log::ERR) << "While SPI write: " << e.what();
		return OpcUa_Bad; // TODO add better error handling
	}

	return OpcUa_Good;
}


// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333



}


