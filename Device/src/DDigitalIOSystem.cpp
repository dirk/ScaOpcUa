
/* This is device body stub */

#include <sstream>

#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DDigitalIOSystem.h>
#include <ASDigitalIOSystem.h>

#include <LogIt.h>

#include <DDigitalIO.h>




namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DDigitalIOSystem::DDigitalIOSystem (
    const Configuration::DigitalIOSystem & config,
    Parent_DDigitalIOSystem * parent
):
    Base_DDigitalIOSystem( config, parent),
    m_sca( parent->sca() )

/* fill up constructor initialization list here */
{

	// compute the mask
	uint32_t directionMask = 0;
	BOOST_FOREACH( const Configuration::DigitalIO & dio, config.DigitalIO() )
	{
		if (not dio.isInput())
		{
			LOG(Log::INF) << "Adding pin number " << (int)dio.id() << " to the direction mask ";
			directionMask |= 1 << dio.id();
		}
	}

	LOG(Log::INF) << "The GPIO direction mask is: " << std::hex << directionMask;
	sca().gpio().setRegister(::Sca::Constants::GpioRegisters::Direction, directionMask);
}

/* sample dtr */
DDigitalIOSystem::~DDigitalIOSystem ()
{
}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DDigitalIOSystem::readDiagnostics (
    UaString &value,
    UaDateTime &sourceTime
)
{
	try
	{
		std::stringstream out;
		out << "direction: " << std::hex << sca().gpio().getRegister( ::Sca::Constants::GpioRegisters::Direction );
		out << " datain: " << std::hex << sca().gpio().getRegister( ::Sca::Constants::GpioRegisters::DataIn );
		out << " dataout: " << std::hex << sca().gpio().getRegister( ::Sca::Constants::GpioRegisters::DataOut );
		value = UaString( out.str().c_str() );
	    sourceTime = UaDateTime::now();
	    return OpcUa_Good;

	}
	catch (...)
	{
		return OpcUa_Bad;
	}

}



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333


void DDigitalIOSystem::setAllToOutput()
{
	m_sca.gpio().setRegister(::Sca::Constants::GpioRegisters::Direction,0x11111111);
}
void DDigitalIOSystem::setAllToInput()
{
	m_sca.gpio().setRegister(::Sca::Constants::GpioRegisters::Direction,0);
}
void DDigitalIOSystem::setAllOutTo1()
{
	m_sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut,0x11111111);
}
void DDigitalIOSystem::setAllOutTo0()
{
	m_sca.gpio().setRegister(::Sca::Constants::GpioRegisters::DataOut,0);
}
unsigned int DDigitalIOSystem::readAllDataIn()
{
	return m_sca.gpio().getRegister(::Sca::Constants::GpioRegisters::DataIn);
}


}


