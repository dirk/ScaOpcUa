
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DDacOutput.h>
#include <ASDacOutput.h>

#include <LogIt.h>



namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DDacOutput::DDacOutput (
    const Configuration::DacOutput & config,
    Parent_DDacOutput * parent
):
    Base_DDacOutput( config, parent),
    m_sca ( parent->sca())

/* fill up constructor initialization list here */
{
    std::string id = config.id();
    /* fill up constructor body here */
    if (id == "A")
        m_id = Sca::Constants::Dac::DAC_A;
    else if (id == "B")
        m_id = Sca::Constants::Dac::DAC_B;
    else if (id == "C")
        m_id = Sca::Constants::Dac::DAC_C;
    else if (id == "D")
        m_id = Sca::Constants::Dac::DAC_D;
    else
        throw std::runtime_error("DAC ID invalid, should be one of: A, B, C, D");

}

/* sample dtr */
DDacOutput::~DDacOutput ()
{
}

/* delegators for cachevariables and externalvariables */

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DDacOutput::readVoltage (
    OpcUa_Double &value,
    UaDateTime &sourceTime
)
{
    sourceTime = UaDateTime::now();
    try
    {
        value = sca().dac().getVoltageEstimateAtRegister( m_id );
        return OpcUa_Good;
    }
    catch (std::exception &e)
    {
        LOG(Log::ERR) << "In reading back the DAC output: " << e.what();
        return OpcUa_Bad; // TODO refine error message
    }
}

/* ASYNCHRONOUS !! -- this executes as separate thread (job from ThreadPool) */
UaStatus DDacOutput::writeVoltage (
    OpcUa_Double &value
)
{
    try
    {
        sca().dac().setVoltageOnChannel( m_id, value );
    }
    catch (std::exception &e)
    {
        LOG(Log::ERR) << "In setting DAC output: " << e.what();
        return OpcUa_Bad; // TODO refine error message
    }

    return OpcUa_Good;
}



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333




}


