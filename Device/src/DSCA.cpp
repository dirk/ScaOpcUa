
/* This is device body stub */


#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DSCA.h>
#include <ASSCA.h>

#include <Hdlc/Backend.h>




namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DSCA::DSCA (
    const Configuration::SCA & config,
    Parent_DSCA * parent
):
    Base_DSCA( config, parent),
	m_sca( config.address() )

/* fill up constructor initialization list here */
{
    /* fill up constructor body here */
}

/* sample dtr */
DSCA::~DSCA ()
{
}

/* delegators for cachevariables and externalvariables */



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

void DSCA::publishId ()
{
    getAddressSpaceLink()->setId( sca().getChipId(), OpcUa_Good );
}

void DSCA::updateStats () const
{
    Hdlc::BackendStatistics bs = sca().getBackendStatistics();
    getAddressSpaceLink()->setNumberRequests(bs.numberFramesSent, OpcUa_Good);
    getAddressSpaceLink()->setNumberReplies(bs.numberFramesReceived, OpcUa_Good);
    getAddressSpaceLink()->setLastReplySecondsAgo(bs.lastReplySecondsAgo, OpcUa_Good);
}

}


