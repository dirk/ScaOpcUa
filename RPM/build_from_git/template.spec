%define name OpcUaSca
%define _topdir %(echo $PWD)
%define _tmpdir %{_topdir}/tmp
%define PREFIX /opt/%{name}

AutoReqProv: yes 
Summary: %{name}
Name: %{name}
Version: %{version}
Release: %{release}
Source0: checkout.tar.gz
License: zlib/libpng license
Group: Development/Application
BuildRoot: %{_topdir}/BUILDROOT/%{name}-%{version}
BuildArch: x86_64
Prefix: %{PREFIX}
Vendor: CERN 

%description
OPC-UA for GBT-SCA.
https://gitlab.cern.ch/atlas-dcs-opcua-servers/ScaOpcUa

%prep
echo ">>> setup tag" 
echo %{name}

%setup -n checkout 

%build
echo "--- Build ---"
scl enable devtoolset-3 "python quasar.py build config-toolkit154.cmake"


%install
echo "--- Install (don't confuse with installation; nothing is installed on your system now in fact...) ---"
INSTALLED_DIR=%{buildroot}/%{PREFIX}/bin
/bin/mkdir -p $INSTALLED_DIR 
/bin/cp bin/OpcUaServer                                         $INSTALLED_DIR
/bin/cp bin/ServerConfig.xml                                    $INSTALLED_DIR/ServerConfig.xml
/bin/cp Configuration/Configuration.xsd                         $INSTALLED_DIR 

%pre
echo "Pre-install: nothing to do"

%post
echo "Generating OPC UA Server Certificate..."
%{PREFIX}/bin/OpcUaServer --create_certificate


%preun


# 
# Hint: if your server installs any shared objects, you should run ldconfig here. 
#

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root)
%{PREFIX}

%changelog
